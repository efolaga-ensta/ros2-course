# Pre requisite

This course has been prepared using ROS 2 foxy version as a consequence, it is required from you to have Ubuntu 20.04 (or any variation),or later as OS. 

# Install ROS 2 foxy

Using your favourite terminal, clone this repository wherever you like using the following command:

``` sh
git clone ...
```

Then opend the cloned repository/folder using: 

``` sh
cd ros2-course
```

In this folder you will find a file named _ros2_ez_install.sh_ . Open it with any text reader to see what it does. Make it executable using:

``` sh
chmod +x ros2-ez-install.sh
```

Then execute it with:

``` sh
./ros2-ez-install.sh
```

# Check your install





